import { LitElement, html } from 'lit-element';

class DemoView extends LitElement {
  render() {
    return html` 
      <p>gitlab-page-demo with lit element</p>
    `;
  }
}

customElements.define('demo-view', DemoView);